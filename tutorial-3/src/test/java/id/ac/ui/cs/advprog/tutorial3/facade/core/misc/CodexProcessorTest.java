package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.CodexProcessor;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.PimTransformation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CodexProcessorTest {

    CodexProcessor codexProcessor = new CodexProcessor();

    @Test
    public void testCodexProcessorToRunicCodex() {
        String text = codexProcessor.toRunicCodex("Kuliah ini bukan perlombaan membuat Web canggih");
        assertEquals("@NC$@J@%(CM.$*AyX]:);y]C&M*[Vdm(tG)HS+)/ySCB,D!", text);
    }

    @Test
    public void testCodexProcessorToAlphaCodex() {
        String code = codexProcessor.toAlphaCodex("@NC$@J@%(CM.$*AyX]:);y]C&M*[Vdm(tG)HS+)/ySCB,D!");
        assertEquals("Kuliah ini bukan perlombaan membuat Web canggih", code);
    }
}
