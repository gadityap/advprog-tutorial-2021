package id.ac.ui.cs.advprog.tutorial3;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial3ApplicationTests {
    @Test
    void contextLoads() {
    }

    //Only to cover main() invocation.
    @Test
    public void main() {
        Tutorial3Application.main(new String[] {});
    }
}
