package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import net.bytebuddy.implementation.bytecode.Throw;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class CodexTranslatorTest {
    @Test
    public void testCodexHasTranslateStaticMethod() throws Exception {
        Class<?> translatorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator");
        Method translate = translatorClass.getDeclaredMethod("translate", Spell.class, Codex.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testCodexTranslateAlphaToRunicProperly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Codex targetCodex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "eJcnBJ_JZz_._DxZM_MX_J_KsJLaNdnMb_MX_cXBvx_XAB_NDXBz";

        Spell result = CodexTranslator.translate(spell, targetCodex);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCodexTranslateThrowsExceptionIfCodexDifferentSize() throws Exception {
        Constructor<RunicCodex> c = (Constructor<RunicCodex>) RunicCodex.class.getDeclaredConstructors()[0];
        c.setAccessible(true);
        RunicCodex codex = (RunicCodex) c.newInstance();
        c.setAccessible(false);

        Field field = codex.getClass().getSuperclass().getDeclaredField("charArr");
        field.setAccessible(true);
        field.set(codex, new char[1]);
        field.setAccessible(false);

        Spell spell = new Spell("lmao", AlphaCodex.getInstance());
        Throwable t = null;
        try {
            CodexTranslator.translate(spell, codex);
        } catch (Throwable th) {
            t = th;
        }

        assertTrue(t instanceof IllegalArgumentException);
        assertTrue(t.getMessage().equals("Jumlah karakter pada kedua Codex tidak sama"));
    }

    @Test
    public void testCodexTranslatorIsInstantiable() {
        CodexTranslator co = new CodexTranslator();
        assertTrue(co instanceof CodexTranslator);
    }
}
