package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PimTransformationTest {
    private Class<?> celestialClass;

    @BeforeEach
    public void setup() throws Exception {
        celestialClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.PimTransformation");
    }

    @Test
    public void testPimHasEncodeMethod() throws Exception {
        Method translate = celestialClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPimEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ainqziHivlHQH4mv1H1wHiHjtiks0uq1pH1wHnwzomHw2zH04wzl";

        Spell result = new PimTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPimEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ainqziHivlHQH4mv1H1wHiHjtiks0uq1pH1wHnwzomHw2zH04wzl";

        Spell result = new PimTransformation("SafiraEmyra").encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPimHasDecodeMethod() throws Exception {
        Method translate = celestialClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPimDecodesCorrectly() throws Exception {
        String text = "ainqziHivlHQH4mv1H1wHiHjtiks0uq1pH1wHnwzomHw2zH04wzl";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new PimTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPimDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "ainqziHivlHQH4mv1H1wHiHjtiks0uq1pH1wHnwzomHw2zH04wzl";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new PimTransformation("SafiraEmyra").decode(spell);
        assertEquals(expected, result.getText());
    }
}
