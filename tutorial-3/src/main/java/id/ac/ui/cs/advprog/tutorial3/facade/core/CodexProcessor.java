package id.ac.ui.cs.advprog.tutorial3.facade.core;

import com.sun.tools.javac.jvm.Code;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.PimTransformation;
import org.springframework.stereotype.Component;

@Component
public class CodexProcessor {
    private AbyssalTransformation abyssalTransformation;
    private CelestialTransformation celestialTransformation;
    private PimTransformation pimTransformation;

    public CodexProcessor() {
        this.abyssalTransformation = new AbyssalTransformation();
        this.celestialTransformation = new CelestialTransformation();
        this.pimTransformation = new PimTransformation();
    }

    public String toRunicCodex(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = this.celestialTransformation.encode(spell);
        spell = this.abyssalTransformation.encode(spell);
        spell = this.pimTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell.getText();
    }

    public String toAlphaCodex(String text) {
        Spell spell = new Spell(text, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = this.pimTransformation.decode(spell);
        spell = this.abyssalTransformation.decode(spell);
        spell = this.celestialTransformation.decode(spell);
        return spell.getText();
    }
}
