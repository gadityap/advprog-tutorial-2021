package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini mengimplementasikan caesar cipher.
 */
public class PimTransformation {
    private int shift = 0;

    public PimTransformation(String strKey) {
        for (int i = 0; i < strKey.length(); i++) {
            shift += strKey.charAt(i);
        }
        shift %= 20;
    }

    public PimTransformation() {
        this("PimPomPim");
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        int inc = encode ? 1 : -1;
        Codex codex = spell.getCodex();
        int codexLen = codex.getCharSize();
        char[] res = spell.getText().toCharArray();
        for (int i = 0; i < res.length; i++) {
            int newI = codex.getIndex(res[i]) + (shift * inc);
            newI = newI < 0 ? newI + codexLen : newI % codexLen;
            res[i] = codex.getChar(newI);
        }

        return new Spell(new String(res), codex);
    }
}
