package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean hasCharged = false;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        this.hasCharged = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (this.hasCharged) {
            this.hasCharged = false;
            return  "Magic power not enough for large spell";
        } else {
            this.hasCharged = true;
            return this.spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
