package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimed = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(this.isAimed);
    }

    @Override
    public String chargedAttack() {
        if (this.isAimed) {
            this.isAimed = false;
            return "Leaving aim mode shot.";
        } else {
            this.isAimed = true;
            return "Entered aim mode shot.";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
