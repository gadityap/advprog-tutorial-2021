package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Override
    public List<Weapon> findAll() {
        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weaponRepository.save(new BowAdapter(bow));
            }
        }
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = findWeapon(weaponName);
        String msg = String.format("%s attacked with %s (%s attack): \"%s\"",
                weapon.getHolderName(),
                weapon.getName(),
                attackType == 0 ? "normal" : "charged",
                attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack());
        logRepository.addLog( msg );
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }

    @Override
    public Weapon findWeapon(String weaponName) {
        Weapon weapon = this.weaponRepository.findByAlias(weaponName);
        if (weapon == null) {
            Bow bow = this.bowRepository.findByAlias(weaponName);
            if (bow == null) {
                Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
                weapon = new SpellbookAdapter(spellbook);
            } else weapon = new BowAdapter(bow);
        }
        this.weaponRepository.save(weapon);
        return weapon;
    }
}
